from PyQt5.QtWidgets import QPushButton, QMenuBar, QApplication, QAction, qApp, QLabel, QHBoxLayout, \
    QVBoxLayout, QWidget, QLineEdit, QDialog, QMainWindow, QGroupBox, QCheckBox, QSpacerItem, QSizePolicy, \
    QDialogButtonBox, QListWidget, QListWidgetItem
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5.QtCore import Qt, QPoint
import sys
import random
import resources


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # self.dic = {}
        # self.con_dic = {}
        # self.variable = set()
        self.variable = set('1')
        self.total_answers = 0
        self.guessed_so_far = 0

        self.dic_Europe = resources.dic_Europe
        self.dic_Asia = resources.dic_Asia
        self.dic_America = resources.dic_America
        self.dic_Africa = resources.dic_Africa
        self.dic_Australia = resources.dic_Australia
        #
        self.correct_answers = resources.correct_answers
        self.wrong_answers = resources.wrong_answers

        self.init_ui()
        self.new_game()

    def init_ui(self):
        self.font = QFont('Arial', 13)
        self.bold_font = QFont('Arial')
        self.bold_font.setBold(True)
        # Status bar
        # self.statusBar().showMessage(self.show_status())

        # Menu bar
        newGameAction = QAction('&Новая игра', self)
        newGameAction.setShortcut('Ctrl+N')
        newGameAction.setStatusTip('Начать новую игру')
        newGameAction.triggered.connect(self.new_game)

        exitAction = QAction('&Выход', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Закрыть программу')
        exitAction.triggered.connect(qApp.quit)

        settingsAction = QAction('Настройки', self)
        settingsAction.setShortcut('Ctrl+S')
        settingsAction.setStatusTip('Открыть настройки')
        settingsAction.triggered.connect(self.show_settings)

        databaseAction = QAction('Все страны и столицы', self)
        databaseAction.setShortcut('Ctrl+A')
        databaseAction.setStatusTip('Показать все страны и столицы')
        databaseAction.setEnabled(True)
        databaseAction.triggered.connect(self.show_data)

        statisticsAction = QAction('&Статистика', self)
        statisticsAction.setShortcut('Ctrl+D')
        statisticsAction.setStatusTip('Показать статистику за всё время')
        statisticsAction.setEnabled(False)

        aboutAction = QAction('&О программе', self)
        aboutAction.setShortcut('Ctrl+H')
        aboutAction.setStatusTip('Сведения о программе')
        aboutAction.setEnabled(True)
        aboutAction.triggered.connect(self.show_about)

        menubar = self.menuBar()
        filemenu = menubar.addMenu('&Файл')
        filemenu.addAction(newGameAction)
        filemenu.addAction(exitAction)
        filemenu = menubar.addMenu('&Настройки')
        filemenu.addAction(settingsAction)
        filemenu = menubar.addMenu('&Помощь')
        filemenu.addAction(databaseAction)
        filemenu.addAction(statisticsAction)
        filemenu.addAction(aboutAction)

        # Line 1
        self.flag_label = QLabel()
        self.flag_label.setVisible(True)
        self.flag_label.setAlignment(Qt.AlignHCenter)
        # self.new_button = QPushButton('Новая игра')
        # self.new_button.clicked.connect(self.new_game)
        
        # Line 2
        self.country_label = QLabel('')
        self.country_label.setAlignment(Qt.AlignHCenter)
        self.country_label.setFont(self.font)

        # Line 3
        hbox = QHBoxLayout()
        self.input_lineEdit = QLineEdit()
        # self.input_lineEdit.returnPressed.connect(self.check_button.click)
        self.hint_button = QPushButton('?')
        self.hint_button.clicked.connect(self.print_debug_info)
        hbox.addWidget(self.input_lineEdit)
        hbox.addWidget(self.hint_button)

        # Line 4
        hbox1 = QHBoxLayout()
        self.check_button = QPushButton('Проверить')
        self.check_button.setFont(self.font)
        self.check_button.clicked.connect(self.check_if_correct)
        self.check_button.setAutoDefault(True)

        self.next_button = QPushButton('Далее')
        self.next_button.setFont(self.font)
        self.next_button.clicked.connect(self.next_country)
        
        hbox1.addWidget(self.check_button)
        hbox1.addWidget(self.next_button)
        

        # Line 5
        self.comment_label = QLabel('')
        self.comment_label.setAlignment(Qt.AlignCenter)
        self.comment_label.setFont(self.font) 

        # Adding widgets
        vbox = QVBoxLayout()
        vbox.addWidget(self.flag_label)
        # vbox.addWidget(self.new_button)
        vbox.addWidget(self.country_label)
        vbox.addLayout(hbox)
        vbox.addLayout(hbox1)
        vbox.addWidget(self.comment_label)

        widget = QWidget()
        widget.setLayout(vbox)
        self.setCentralWidget(widget)

    # TODO: imlement correct exit
    # TODO: make better layout (think CSS)
    # TODO: add settings for flags and countries' names

    def show_about(self):
        about_window = QDialog(self)
        about_window.setWindowTitle('О программе')
        about_window.setWindowModality(Qt.WindowModal)
        about_window.resize(200, 200)
        about_label = QLabel('Угадай столицу©\nВерсия 0.1 beta\nJoseph Residence, 2017')
        vbox = QVBoxLayout()
        vbox.addWidget(about_label)
        about_window.setLayout(vbox)
        about_window.show()

    def show_data(self):
        data_window = QDialog(self)
        data_window.setWindowTitle('Все страны и столицы')
        data_window.setWindowModality(Qt.WindowModal)
        data_window.resize(314, 350)
        self.data_list = QListWidget()

        Europe_item = QListWidgetItem('Европа')
        self.populate_to_help(Europe_item, self.dic_Europe)

        self.data_list.addItem('')
        Asia_item = QListWidgetItem('Азия')
        self.populate_to_help(Asia_item, self.dic_Asia)

        self.data_list.addItem('')
        America_item = QListWidgetItem('Америка')
        self.populate_to_help(America_item, self.dic_America)

        self.data_list.addItem('')
        Africa_item = QListWidgetItem('Африка')
        self.populate_to_help(Africa_item, self.dic_Africa)

        self.data_list.addItem('')
        Australia_item = QListWidgetItem('Австралия и Океания')
        self.populate_to_help(Australia_item, self.dic_Australia)

        vbox = QVBoxLayout()
        vbox.addWidget(self.data_list)
        data_window.setLayout(vbox)
        data_window.show()
        
    def populate_to_help(self, item, dic):
        item.setFont(self.bold_font)
        self.data_list.addItem(item)
        for k, v in sorted(dic.items()):
            self.data_list.addItem('{} - {}'.format(k, v))

    def show_settings(self):
        settings_window = QDialog(self)
        settings_window.setWindowTitle('Настройки')
        settings_window.setWindowModality(Qt.WindowModal)
        # settings_window.setAttribute(Qt.WA_DeleteOnClose, True)
        settings_window.resize(200, 100)

        continents_group = QGroupBox('Выбрать регионы:')
        self.cb1 = QCheckBox('Европа')
        self.cb2 = QCheckBox('Азия')
        self.cb3 = QCheckBox('Америка')
        self.cb4 = QCheckBox('Африка')
        self.cb5 = QCheckBox('Австралия')
        if '1' in self.variable:
            self.cb1.toggle()
        if '2' in self.variable:
            self.cb2.toggle()
        if '3' in self.variable:
            self.cb3.toggle()
        if '4' in self.variable:
            self.cb4.toggle()
        if '5' in self.variable:
            self.cb5.toggle()
        self.cb1.stateChanged.connect(self.set_variable_1)
        self.cb2.stateChanged.connect(self.set_variable_2)
        self.cb3.stateChanged.connect(self.set_variable_3)
        self.cb4.stateChanged.connect(self.set_variable_4)
        self.cb5.stateChanged.connect(self.set_variable_5)

        mainbox = QVBoxLayout()
        vbox = QVBoxLayout()
        vbox.addWidget(self.cb1)
        vbox.addWidget(self.cb2)
        vbox.addWidget(self.cb3)
        vbox.addWidget(self.cb4)
        vbox.addWidget(self.cb5)

        continents_group.setLayout(vbox)
        mainbox.addWidget(continents_group)

        buttons = QDialogButtonBox(QDialogButtonBox.Ok, Qt.Horizontal, self)
        # buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, Qt.Horizontal, self)
        buttons.accepted.connect(settings_window.accept)
        buttons.accepted.connect(self.generate_con_dic)
        # buttons.accepted.connect(self.print_dic)
        # buttons.accepted.connect(self.generate_con_dic)
        # buttons.rejected.connect(settings_window.reject)
        mainbox.addWidget(buttons)
        settings_window.setLayout(mainbox)
        settings_window.show()

    def set_variable_1(self, state):
        if state == Qt.Checked:
            self.variable.add('1')
        else:
            self.variable.discard('1')
        print(self.variable)

    def set_variable_2(self, state):
        if state == Qt.Checked:
            self.variable.add('2')
        else:
            self.variable.discard('2')
        print(self.variable)

    def set_variable_3(self, state):
        if state == Qt.Checked:
            self.variable.add('3')
        else:
            self.variable.discard('3')
        print(self.variable)

    def set_variable_4(self, state):
        if state == Qt.Checked:
            self.variable.add('4')
        else:
            self.variable.discard('4')
        print(self.variable)

    def set_variable_5(self, state):
        if state == Qt.Checked:
            self.variable.add('5')
        else:
            self.variable.discard('5')
        print(self.variable)

    def generate_con_dic(self):
        self.con_dic = {}
        if '1' in self.variable:
           self.con_dic.update(self.dic_Europe)
        if '2' in self.variable:
            self.con_dic.update(self.dic_Asia)
        if '3' in self.variable:
            self.con_dic.update(self.dic_America)
        if '4' in self.variable:
            self.con_dic.update(self.dic_Africa)
        if '5' in self.variable:
            self.con_dic.update(self.dic_Australia)
##        if '1' in self.variable:
##            self.con_dic = {**self.con_dic, **self.dic_Europe}
##        if '2' in self.variable:
##            self.con_dic = {**self.con_dic, **self.dic_Asia}
##        if '3' in self.variable:
##            self.con_dic = {**self.con_dic, **self.dic_America}
##        if '4' in self.variable:
##            self.con_dic = {**self.con_dic, **self.dic_Africa}
##        if '5' in self.variable:
##           self.con_dic = {**self.con_dic, **self.dic_Australia}

    def print_debug_info(self):
        self.generate_con_dic()
        print(self.variable)
        print(self.con_dic)

    def next_country(self):
        self.generate_con_dic()
        self.total_answers += 1
        self.randomize_country()
        self.country_label.setText(self.random_country)
        self.show_flag()
        self.hint_button.setToolTip(self.show_hint())
        self.statusBar().showMessage(self.show_status())
        
##        self.randomize_country()
##        self.country_label.setText(self.random_country)
##        self.hint_button.setToolTip(self.show_hint())
##        self.statusBar().showMessage(self.show_status())
        self.input_lineEdit.clear()
        self.check_button.setEnabled(True)

    def check_if_correct(self):
        self.guess_input = self.input_lineEdit.text()
        print(self.random_country)
        if self.guess_input.lower() == self.con_dic[self.random_country].lower():
            self.comment_label.setText(self.correct_answer())
            self.comment_label.setStyleSheet('QLabel {color: green;}')
            self.guessed_so_far += 1
            self.statusBar().showMessage(self.show_status())
        else:
            self.comment_label.setText(self.wrong_answer())
            self.comment_label.setStyleSheet('QLabel {color: red;}')
            self.statusBar().showMessage(self.show_status())
        self.check_button.setEnabled(False)


    def correct_answer(self):
        answer = random.choice(self.correct_answers)
        return answer
        
    def wrong_answer(self):
        answer = (random.choice(self.wrong_answers)) + \
                 '\nПравильный ответ - ' + self.con_dic[self.random_country] + '.'
        return answer

    def show_hint(self):
        return "Вторая буква - " + str(self.con_dic[self.random_country][1])

    def show_status(self):
        self.status = 'Угадано ' + str(self.guessed_so_far) + ' из ' + str(self.total_answers)
        return self.status

    def new_game(self):
        self.generate_con_dic()
        self.randomize_country()
        print(self.random_country)
        self.country_label.setText(self.random_country)
        self.show_flag()
        self.hint_button.setToolTip(self.show_hint())
        self.input_lineEdit.clear()
        self.guessed_so_far = 0
        self.total_answers = 0
        self.statusBar().showMessage(self.show_status())

    def randomize_country(self):
        self.random_country = random.choice(list(self.con_dic.keys()))

    def get_random_country(self):
        self.randomize_country()
        return self.random_country

    def show_flag(self):
        self.flag_name = './Flags/' + self.random_country + '.png'
        self.flag_label.setPixmap(QPixmap(self.flag_name))
        print(self.flag_name)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.resize(300, 400)
    window.setWindowTitle('Угадай столицу')

    # Show custom icon for window and for the app.
    # ico = QIcon('icon.png')
    # window.setWindowIcon(ico)
    # app.setWindowIcon(ico)

    window.show()
    sys.exit(app.exec_())
